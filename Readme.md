# Open Information Extraction
*Made together with Naor Matalon for the course 'Natural language processing' at the Hebrew University.* 

A simple open information extraction system that takes text from Wikipedia and extracts triplets of (Subject, Relation, Object), where each of them is a span of text. 

## Methods
Two extraction methods are implemented and evaluated
*	Part Of Speech(POS) based extraction
*	Dependency tree based extraction
*	The evaluation procedure produces random discovered triplets for manual evaluation.

### Required Libraries:
*	spacy: https://github.com/explosion/spaCy
*	wikipedia: https://github.com/goldsmith/Wikipedia
