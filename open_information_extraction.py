import itertools
import random
import spacy
import wikipedia

PROPN = "PROPN"
PUNCT = "PUNCT"
VERB = "VERB"
ADP = "ADP"
PN = "PROPN"
COMPOUND = "compound"
NSUBJ = "nsubj"
DOBJ = "dobj"
PREP = "prep"
POBJ = "pobj"


def depenency_extractor(Doc):
    """
    extract triplets of with structure (Subject, Relation, Object) from the document using dependency tree information
    :param Doc: the analyzed document (nlp_model)
    :return: a list of the triplets where each triplet is a list of 3 sets [Subject, Relation, Object].
    """
    triplets = []
    for sent in Doc.sents:
        proper_noun_heads = []
        proper_nouns = dict()
        for token in sent:
            if token.pos_ == PN and token.dep_ != COMPOUND:
                proper_noun_heads.append(token)
                proper_noun = {token.text}
                proper_noun.update(set([child.text for child in token.children if child.dep_ == COMPOUND]))
                proper_nouns[token] = proper_noun
        for tuple in itertools.product(proper_noun_heads, proper_noun_heads):
            h1, h2 = tuple[0], tuple[1]
            if h1.head == h2.head:
                if h1.dep_ == NSUBJ and h2.dep_ == DOBJ:
                    triplets.append([proper_nouns[h1], {h1.head.text}, proper_nouns[h2]])
            elif h1.head == h2.head.head:
                h, h_tag = h1.head, h2.head
                if h1.dep_ == NSUBJ and h_tag.dep_ == PREP and h2.dep_ == POBJ:
                    triplets.append([proper_nouns[h1], {h.text + " " + h_tag.text}, proper_nouns[h2]])
    return triplets


def pos_extractor(analyzed_page):
    """
    extract triplets of with structure (Subject, Relation, Object) from the document using POS tag information
    :param analyzed_page: the analyzed document (nlp_model)
    :return: a list of the triplets where each triplet is a list of [Subject, Relation, Object].
    """
    triplets = []
    for sentence in analyzed_page.sents:
        is_relation_detected, is_subject_detected, object, relation, sentence_size, subject, word_idx = \
            initialize_parameters_for_sentence(sentence)
        while word_idx != sentence_size:
            if sentence[word_idx].pos_ == PROPN:
                is_relation_detected, is_subject_detected, relation, word_idx, subject = \
                    proper_noun_found_case(is_relation_detected, is_subject_detected, object, relation, sentence,
                                           sentence_size, subject, triplets, word_idx)
            else:
                is_relation_detected, relation = relation_found_case(is_relation_detected, is_subject_detected,
                                                                     relation, sentence, word_idx)

                # if we found first PREPN and then a PUNCT, we start all over again
                is_relation_detected, is_subject_detected, object, relation, subject = punctuation_found_case(
                    is_relation_detected, is_subject_detected, object, relation, sentence, subject, word_idx)

                word_idx += 1
    return triplets


def punctuation_found_case(is_relation_detected, is_subject_detected, object, relation, sentence, subject, word_idx):
    """
    manage the case which a punctuation is found
    :param is_relation_detected: bool - true if relation detected, false otherwise
    :param is_subject_detected: bool - true if subject detected, false otherwise
    :param object: the object string
    :param relation: the relation string
    :param sentence: the current sentence
    :param subject: the subject string
    :param word_idx: the index of the word in the current sentence
    :return: the updated parameters
    """
    if sentence[word_idx].pos_ == PUNCT and is_subject_detected:
        is_subject_detected = is_relation_detected = False
        subject = relation = object = ""
    return is_relation_detected, is_subject_detected, object, relation, subject


def relation_found_case(is_relation_detected, is_subject_detected, relation, sentence, word_idx):
    """
    manage the case which a relation(verb or adposition) is found
    :param is_relation_detected: bool - true if relation detected, false otherwise
    :param is_subject_detected: bool - true if subject detected, false otherwise
    :param relation: the relation string
    :param sentence: the current sentence
    :param word_idx: the index of the word in the current sentence
    :return: the updated parameters
    """
    if (sentence[word_idx].pos_ == VERB or (sentence[word_idx].pos_ == ADP and is_relation_detected)) and \
            is_subject_detected:
        is_relation_detected = True
        relation += sentence[word_idx].text + " "
    return is_relation_detected, relation


def proper_noun_found_case(is_relation_detected, is_subject_detected, object, relation, sentence, sentence_size, subject,
                           triplets, word_idx):
    """
    manage the case which a proper noun is found
    :param is_relation_detected: bool - true if relation detected, false otherwise
    :param is_subject_detected: bool - true if subject detected, false otherwise
    :param object: the object string
    :param relation: the relation string
    :param sentence: the current sentence
    :param sentence_size: the sentence size
    :param subject: the subject string
    :param triplets: the list of trplets
    :param word_idx: the index of the word in the current sentence
    :return: the updated parameters
    """
    # if it is the first PROPN
    if not is_subject_detected:
        is_subject_detected = True
        while word_idx != sentence_size and sentence[word_idx].pos_ == PROPN:
            subject += sentence[word_idx].text + " "
            word_idx += 1
        # else, if it is the second PREPN after detecting a verb
    elif is_relation_detected:
        while word_idx != sentence_size and sentence[word_idx].pos_ == PROPN:
            object += sentence[word_idx].text + " "
            word_idx += 1
        triplets.append([subject[:-1], relation[:-1], object[:-1]])
        is_relation_detected = False
        subject = object
        relation = object = ""
    else:
        word_idx += 1
    return is_relation_detected, is_subject_detected, relation, word_idx, subject


def initialize_parameters_for_sentence(sentence):
    """
    initialie parameters for each new sentence
    :param sentence: the current sentence
    :return: empty string("") for subject, relation and object, the sentence size and zero the word_idx
    """
    is_subject_detected = is_relation_detected = False
    subject = relation = object = ""
    word_idx = 0
    sentence_size = len(sentence)
    return is_relation_detected, is_subject_detected, object, relation, sentence_size, subject, word_idx


def evaluate_extractors():
    """
    evaluate both extraction methods (POS and Dep) on the Wikipedia pages as instructed in the ex. description
    """
    nlp_model = spacy.load('en')
    queries = ["Donald Trump", "Ruth Bader Ginsburg", "J.K.Rowling"]
    k = 5  # num of random sentences per query
    for query in queries:
        page = wikipedia.page(query).content
        analyzed_page = nlp_model(page)
        pos_triplets = pos_extractor(analyzed_page)
        dep_triplets = depenency_extractor(analyzed_page)
        pos_random_sample = random.sample(pos_triplets, k)
        dep_ramdom_sample = random.sample(dep_triplets, k)

        print("- " * 15 + query.capitalize() + " -" * 15)

        print("# " * 3 + "POS" + " #" * 3)
        print("The POS extractor got " + str(len(pos_triplets)) + " triplets")
        print("Random samples for the query " + query + ":")
        print(*pos_random_sample, sep="\n", end="\n\n")

        print("# " * 3 + "DEPENDENCY" + " #" * 3)
        print("The Dependency extractor got " + str(len(dep_triplets)) + " triplets.")
        print("Random samples for the query " + query + ":")
        print(*dep_ramdom_sample, sep="\n", end="\n\n\n")


if __name__ == '__main__':
    evaluate_extractors()


